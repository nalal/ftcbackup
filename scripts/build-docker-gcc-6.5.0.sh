#!/bin/bash

# Init some scripting shit
GCC_VER=6.5.0
DOCKER_LOCAL_IMG=registry.gitlab.com/nalal/ftcbackup/ftc_gcc:$GCC_VER\_local

# Build the correct docker image locally
docker build --target builder --tag $DOCKER_LOCAL_IMG -f docker/ftc_gcc-$GCC_VER.Dockerfile .

# Clear the Screen
clear

docker run \
    --rm \
    -v ${PWD}:/src/ \
		--user $(id -u):$(id -g) \
    $DOCKER_LOCAL_IMG \
    sh -c ' \
		cd src \
		&& echo "====================" \
		&& gcc --version \
		&& g++ --version \
		&& echo "====================\n" \
		&& make backUp'
