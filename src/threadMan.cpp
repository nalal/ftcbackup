#include <iostream>
#include <string>
#include <future> 
#include <thread>
#include <unistd.h>
#include "threadMan.h"
#include "webHook.h"



#ifndef _MAX_THREADING_OVERRIDE
#define _MAX_THREADING_OVERRIDE
#endif

#ifndef _NULL_MAX_THREADING
#define _NULL_MAX_THREADING
#endif



using namespace std;

int threadLimit()
{
	int threadL;
	if(!_NULL_MAX_THREADING)
	{
		threadL = _MAX_THREADING_OVERRIDE;
	}
	else
	{
		threadL = 0;
	}
	return threadL;
}

int threadLimitVal = threadLimit();

//Returns CPU thread count
int numCPU()
{
	int numtCPU = thread::hardware_concurrency();
	return numtCPU;
}

void threadLimitSet(int threads)
{
	threadLimitVal = threads;
}

//return core limit
int limitReturn()
{
	return threadLimit();
}

//Start new thread
void loadThread(const char *ins, string func, bool check)
{
	if(func.compare("message") == 0)
	{
		async (launch::async,relayMsg,ins,check);
	}
	else
	{
		cout << "MAX_THREADING_OVERRIDE set at preprocess, limit is " 
		<< threadLimit() << endl;
		//Catch invalid func
		cout << "Async called with invalid function " << func << endl;
	}
}
