/* 
 * =============================================================================
 * APPLICATION NAME:
 * Back Copy
 * 
 * AUTHOR NAME:
 * Nac/Nalal/Noah
 * 
 * AUTHOR GROUP/CORP:
 * FTC Hosting
 * 
 * INIT DATE+TIME:
 * 13th of May, 2019, 6:15PM
 * 
 * =============================================================================
 */
 
#include <ctime>
#include <iostream>
#include <fstream>
#include <experimental/filesystem>
#include <sstream>
#include <unistd.h> 
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>

#include "threadMan.h"

using namespace std;
namespace fs = std::experimental::filesystem;

string versionNo = "A_0.0.1";
bool noLog;
void relayMsgMake(const char *m)
{
	loadThread(m, "message", noLog);
}

int getDay()
{
	//Get current day (number in month) and return it as an int
	time_t rawtime;
	struct tm * timeinfo;
	char buffer[3];
	time (&rawtime);
	timeinfo = localtime(&rawtime);
	strftime(buffer,sizeof(buffer),"%d",timeinfo);
	int dayNum(stoi(buffer));
	return dayNum;
}

int getMonth()
{
	//Get current month and return it as an int
	time_t rawtime;
	struct tm * timeinfo;
	char buffer[3];
	time (&rawtime);
	timeinfo = localtime(&rawtime);
	strftime(buffer,sizeof(buffer),"%m",timeinfo);
	int dayNum(stoi(buffer));
	return dayNum;
}

int getYear()
{
	//Get current year and return it as an int
	time_t rawtime;
	struct tm * timeinfo;
	char buffer[3];
	time (&rawtime);
	timeinfo = localtime(&rawtime);
	strftime(buffer,sizeof(buffer),"%y",timeinfo);
	int dayNum(stoi(buffer));
	return dayNum;
}
//Get current date in dateform
string getDateS()
{
	//Get todays date from functions and return it as a string
	string date(to_string(getDay()) + "-" + to_string(getMonth()) + "-" 
	+ to_string(getYear()));
	return date;
}
//Rip dateform string into individual ints based on '-' spacers
int* getDateFromString(string s)
{
	int dateDayInt;
	int dateMonthInt;
	int dateYearInt;
	
	
	int dateSeg(0);
	string holder;
	for(int i = 0; i <= s.length(); i++)
	{
		if(isdigit(s[i]) and i != s.length())
		{
			if(dateSeg == 0)
			{
				holder = holder + s[i];
			}
			else if(dateSeg == 1)
			{
				holder = holder + s[i];
			}
			else
			{
				holder = holder + s[i];
			}
		}
		else
		{
			if(dateSeg <= 2)
			{
				if (dateSeg == 0)
				{
					dateDayInt = stoi(holder);
				}
				else if (dateSeg == 1)
				{
					dateMonthInt = stoi(holder);
				}
				else if (dateSeg == 2)
				{
					dateYearInt = stoi(holder);
				}
				holder = "";
				dateSeg++;
			}
			else
			{
				dateSeg = 0;
			}
		}
	
	}
	int* segVals = new int[3];
	segVals[1] = dateDayInt;
	segVals[2] = dateMonthInt;
	segVals[3] = dateYearInt;
	return segVals;
}
//Check if name is in proper date format
bool checkName(string s)
{
	if(s.length() > 6 and s.length() < 9)
	{
		for(int i = 0; i <= s.length() - 1; i++)
		{
			//cout << "i is " << s[i] << endl;
			if(!isdigit(s[i]) and s.find("-") == string::npos )
			{
				return false;
			}
		}
		return true;
	}
	else
	{
		return false;
	}
}
//Madlad Standard C FileIO operation to get UID for origin file
int getFSUID(string s)
{
	int ret;
	struct stat sb;
	stat(s.c_str(), &sb);
	ret = sb.st_uid;
	return ret;
}

string getDateRemove()
{
	//Get current date
	int iDay(getDay());
	int iMonth(getMonth());
	int iYear(getYear());
	//Catch if last month was in the previous year
	if(iMonth == 1 and iDay == 1)
	{
		iMonth = 12;
		iYear = iYear - 1;
		iDay = 25;
	}
	else if(iMonth != 1 and iDay == 1)
	{
		iMonth = iMonth - 1;
		iDay = 25;
	}
	else
	{
		iDay = iDay - 1;
	}
	
	//Return date to delete
	string removeDate(to_string(iDay) + "-" + to_string(iMonth) +  "-" 
	+ to_string(iYear));
	return removeDate;
}

int main(int argc, char *argv[])
{
	//Arg vars
	bool clean(true);
	bool clearExists(true);
	noLog = false;
	
	//DIR variable init
	string destFolder;
	string targetFolder;
	
	//Get today's date and date from one month ago
	string todaysDate(getDateS());
	string purge(getDateRemove());
	
	//Setting values for DIR variables
	destFolder = "INVALID";
	targetFolder = "INVALID";
	
	//Check if required args are supplied
	//Get args if there are more than 3 (C++ counts the line to run the program 
	//as an arg)
	string arg;
	for(int a = 1; a < argc; a++)
	{
		arg = argv[a];
		//cout << "arg is " << arg << endl;
		if(arg.compare("-t") == 0 or arg.compare("--target") == 0)
		{
			targetFolder = argv[(a +1)];
			cout << "targetFolder is " << targetFolder << endl;
			a++;
		}
		else if(arg.compare("-d") == 0 or arg.compare("--destination") == 0)
		{
			destFolder = argv[(a +1)];
			cout << "destFolder is " << destFolder << endl;
			a++;
		}
		else if(arg.compare("-n") == 0 or arg.compare("--noclean") == 0)
		{
				clean = false;
				cout << "Noclean given, will skip deleting past date backups." 
				<< endl;
		}
		else if(arg.compare("-q") == 0 or arg.compare("--quiet") == 0)
		{
			noLog = true;
			cout << "Quiet given, will skip discord relay." << endl;
		}
		else if(arg.compare("--version") == 0)
		{
			cout << "FTC Backup Script v" << versionNo << endl 
			<< "AUTHORS: " << endl
			<< "Nac/Nalal/Nacalal/Noah" << endl
			<< "Hotaru/Flutters" << endl;
			return 0;
		}
		else if(arg.compare("--DEBUG") == 0)
		{
			cout << "Total threads " << numCPU() << endl;
			cout << "Thread limit " << limitReturn() << endl;
			return 0;
		}
		else
		{
			cout << "Argument " << arg << " is invalid" << endl;
		}
		//cout << "executed arg " << arg << endl;
	}
	const char *rMsg;
	rMsg = "Backups Started.";
	relayMsgMake(rMsg);
	if(targetFolder == "INVALID" or destFolder == "INVALID")
	{
		cout << "Target or destination missing, halting." << endl;
		cout << "targetFolder " << targetFolder << endl;
		cout << "destFolder " << destFolder << endl;
		return 0;
	}
	//If an insufficient ammount of args is given, halt the program.
	//Print directories to console
	cout << "Copying files from " << targetFolder << endl;
	cout << "Copying files to " << destFolder << todaysDate << endl;	
	
	//Check if given folders actually exist
	if(!fs::exists(destFolder) or !fs::exists(targetFolder))
	{
		rMsg = "ERROR: Backup called but invalid folder(s) were given.";
		relayMsgMake(rMsg);
		cout << "Destination or Target folder does not exist, terminating." 
		<< endl;
		return 0;
	}
	
	//Check if current date already has DIR, remove if clearExists is true
	//This prevents clutter and 'File exists' errors but also prevents wasted 
	//I/O clearing data
	string folderEndName(todaysDate);
	if(fs::exists(destFolder + folderEndName))
	{
		int totalCopies = 0;
		while(fs::exists(destFolder + folderEndName))
		{
			cout << "folder " << folderEndName << 
			" exists, adding and checking." << endl;
			totalCopies++;
			folderEndName = todaysDate + "-" + to_string(totalCopies);
		}
		cout << "folder " << folderEndName << " does not exist, using." 
		<< endl;
	}
	//Create directory with date
	if(!fs::exists(destFolder + todaysDate))
	{
		rMsg = "Executing FileIO operation for backup.";
		relayMsgMake(rMsg);
		
		fs::create_directory(destFolder + folderEndName);
		
		int fileTotalT = 0;
		for(auto& p: fs::recursive_directory_iterator(targetFolder))
		{
			fileTotalT++;
		}
		
		//Copy files to backup DIR
		string srMsg = "Total of " + to_string(fileTotalT) + " files to be"
		" moved.";
		relayMsgMake(srMsg.c_str());
		
		if(fs::exists(destFolder + "backup.sql"))
		{	
			cout << "backup.sql found, copying to " << folderEndName << "sql" 
			<< endl;
			fs::create_directory(destFolder + folderEndName + "sql");
			fs::copy(
				destFolder + "backup.sql", 
				destFolder + folderEndName+ "sql"
			);
			cout << "backup.sql copied" << endl;
		}
		else
		{
			cout << "backup.sql does not exist, skipping..." << endl;
		}
		fs::copy(
			targetFolder, destFolder + folderEndName, 
			fs::copy_options::recursive
		);
		//Get origin folder UID
		string firstFolder;
		cout << "Parsing folders in directory " << targetFolder << endl;
		for(auto& p: fs::directory_iterator(targetFolder))
		{
			cout << p.path() << endl;
			firstFolder = p.path().string();
			break;
		}
		int fileTotalD = 0;
		for(auto& p: fs::recursive_directory_iterator(
			destFolder + folderEndName
		))
		{
			fileTotalD++;
		}
		srMsg = "Moved " + to_string(fileTotalT) + "/" + to_string(fileTotalD) 
		+ " files..";
		relayMsgMake(srMsg.c_str());
		int real = getFSUID(firstFolder);
		cout << "Origin UID is " << real << ", setting output files to match." 
		<< endl;
		//Set initial forlder to correct perms, then recurse into folder and 
		//sub folders doing the same
		chown((destFolder + todaysDate).c_str(), real, real);
		rMsg = "Setting backup folder ownership.";
		relayMsgMake(rMsg);
		for(auto& p: fs::recursive_directory_iterator(destFolder + todaysDate))
		{
			chown(p.path().c_str(), real, real);
		}
	}
	
	cout << "Current date is " << todaysDate << endl;
	cout << "If permitted, removing backups from " << purge << " and earlier." 
	<< endl;
	
	//Check if there is a DIR that is 1 month old and check if clean is true
	//Might move the date from a hard coded val to a config val
	if(fs::exists((destFolder + purge).c_str()) and clean)
	{
		rMsg = "Removing old backups.";
		relayMsgMake(rMsg);
		//If folder from one month ago exists, delete it
		for(auto& p: fs::directory_iterator(destFolder))
		{	
			string pathNameCheck = p.path().stem().string();
			string pathName = p.path().string();
			//cout << "p is " << p.path() << endl;
			//cout << "pathnamecheck is " << pathNameCheck << endl;
			if (checkName(pathNameCheck))
			{
				int* dates = getDateFromString(pathNameCheck);
				cout << "dates[1] " << dates[1] << endl
				<< "dates[2] " << dates[2] << endl
				<< "dates[3] " << dates[3] << endl;
				if(getDay() >= 7)
				{
					if(dates[1] < getDay())
					{
						cout << "removing backup " << pathNameCheck << endl;
						fs::remove_all((pathName).c_str());
					}
				}
				else if(
					getDay() < 7 
					and dates[2] < getMonth() 
					and dates[3] <= getYear()
				){
					cout << "removing backup " << pathNameCheck << endl;
					fs::remove_all((pathName).c_str());
				}
			}
		}
		cout << "Backups from " << purge << " have been cleaned." << endl;
	}
	//If the folder exists but the noclean flag was given, it will do nothing
	else if(fs::exists((destFolder + purge).c_str()) and !clean)
	{
		cout << "noclean flag was used, skipping old file clean regardless of"
		" existance." << endl;
	}
	//If no folder older than 1 month exists, do cout and move to final return
	else
	{
		//If folder from one month ago doesn't exist, notify user
		cout << "Folder with name " << purge << " does not exist in target DIR,"
		" nothing deleted." << endl;
	}
	rMsg = "Backup operation complete.";
	relayMsgMake(rMsg);
	return 0;
}
