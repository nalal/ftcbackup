#pragma once
//Get CPU thread count
int numCPU();

//Changes/overrides thread limit
void threadLimitSet(int threads);

int limitReturn();

//Run function async
void loadThread(const char *ins, std::string func, bool check);
