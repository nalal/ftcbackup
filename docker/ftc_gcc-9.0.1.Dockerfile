ARG BASE_IMAGE=ubuntu:disco-20190515

# Build Project
FROM ${BASE_IMAGE} as builder

LABEL MAINTAINER="MrFlutters <DeriousHD@Gmail.com>"

RUN apt update -qq \
	&& apt install apt-utils -y \
	&& apt install libcurl4-openssl-dev -qq -y  \
	&& apt install gcc-9 -qq -y \
	&& apt install g++-9 -qq -y \
	&& apt install make \
	&& apt clean

RUN ln -s /usr/bin/gcc-9 /usr/bin/gcc \
	&& ln -s /usr/bin/g++-9 /usr/bin/g++

# Run Project
FROM ${BASE_IMAGE} as runtime

LABEL MAINTAINER="MrFlutters <DeriousHD@Gmail.com>"

RUN apt update -qq \
	&& apt install apt-utils -y \
	&& apt install libcurl4-openssl-dev -y  \
	&& apt clean

# For Testing/Debug
FROM builder as testing

LABEL MAINTAINER="MrFlutters <DeriousHD@Gmail.com>"

RUN apt update -qq \
	&& apt install valgrind -y  \
	&& apt clean