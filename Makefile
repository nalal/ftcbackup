prep:
# Make sure output folder exists
ifndef OUTPUT_DIR
$(warning OUTPUT_DIR is not defined, using build/)
OUTPUT_DIR := build
endif

# make sure an output name is set
ifndef OUTPUT_NAME
$(warning OUTPUT_NAME is not defined, using backCopy)
OUTPUT_NAME := backCopy
endif

ifdef MAX_THREADING_OVERRIDE
$(warning MAX_THREADING_OVERRIDE is set, max threads is ${MAX_THREADING_OVERRIDE})
endif

ifndef MAX_THREADING_OVERRIDE
$(warning MAX_THREADING_OVERRIDE is not set, max threads is 4)
# If Max Threads is not set, default to 4
MAX_THREADING_OVERRIDE := 4
endif

ifdef NULL_MAX_THREADING
$(warning NULL_MAX_THREADING is set, program will no longer limit threads)
endif

ifndef NULL_MAX_THREADING
$(warning NULL_MAX_THREADING is set, program will no longer limit threads)
NULL_MAX_THREADING := false
endif

# Grab the gcc version used for compile
BUILD_GCC_VER := $(shell gcc --version | head -n1 | cut -d" " -f4)

###########################################################################
# Above could probably be put into another file, but i had no luck ########
###########################################################################

backUp: prep webHook.o threadMan.o backCopy.o
	g++ ${OUTPUT_DIR}/${BUILD_GCC_VER}/stage/webHook.o ${OUTPUT_DIR}/${BUILD_GCC_VER}/stage/threadMan.o  ${OUTPUT_DIR}/${BUILD_GCC_VER}/stage/backCopy.o -xc++ -lstdc++fs -lstdc++ -lcurl -lpthread -shared-libgcc -o ${OUTPUT_DIR}/${BUILD_GCC_VER}/bin/${OUTPUT_NAME}

backCopy.o: src/backCopy.cpp src/webHook.h src/threadMan.h
	g++ -c src/backCopy.cpp -o ${OUTPUT_DIR}/${BUILD_GCC_VER}/stage/backCopy.o 

threadMan.o: src/threadMan.cpp src/threadMan.h
	g++ -c src/threadMan.cpp -o ${OUTPUT_DIR}/${BUILD_GCC_VER}/stage/threadMan.o -D_MAX_THREADING_OVERRIDE=${MAX_THREADING_OVERRIDE} -D_NULL_MAX_THREADING=${NULL_MAX_THREADING}

webHook.o: src/webHook.cpp src/webHook.h
	mkdir -p ${OUTPUT_DIR}/${BUILD_GCC_VER}/stage ${OUTPUT_DIR}/${BUILD_GCC_VER}/bin
	gcc -c src/webHook.cpp -o ${OUTPUT_DIR}/${BUILD_GCC_VER}/stage/webHook.o 

clean:
	rm ${OUTPUT_DIR}/${BUILD_GCC_VER}/stage/*.o ${OUTPUT_DIR}/${BUILD_GCC_VER}/bin/*
