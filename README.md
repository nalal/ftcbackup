FTCBackupUtility <br>
Used for FTC's internal backups <br>
To build, run: <br>
`make backUp`<br> <br>

Its also possible to build using only docker by running <br>
`./scripts/build-docker-gcc-6.3.0.sh` <br>
or <br>
`./scripts/build-docker-gcc-9.1.0.sh` <br>
**Make sure you run this in the root directory** <br> <br>
**Argument list**<br>
`-t/--target <folder to copy from>` (sets where to copy backup from)<br>
`-d/--destination <folder to copy to>` (sets where to copy backup to)<br>
`-s/--safe` (Will not backup if there is already a backup from today)<br>
`-n/--noclean` (Will not remove out of date backups)<br>
`-q/--quiet` (Will not relay to discord bot)<br>