#!/bin/bash
echo "Making debug build."
if [ -z "$1" ]
then
	echo "No threads given for -j, making in single core."
	make backUp NULL_MAX_THREADING=true
else
	echo "Making with $1 threads"
	make backUp NULL_MAX_THREADING=true -j $1
fi
